# README

This repository is a reproduction of my personal repo. 

The following are the steps to get up and running: 

<h1>1. Install dependencies</h1>

Run `bundle install` to install necessary gems. 

<h1>2. Create private and public keys</h1>

First, navigate the `/tmp` folder and create a `rsaprivkey.pem` and `rsapubkey.pem` file. Once you've done this, you can use the openssl cli tool or navigate to the [this link](https://cryptotools.net/rsagen) to generate keys. 

<h1>3. Create the database</h1>

Start your postgres server and run the following commands:

```
rails db:create
rails db:migrate
```

<h1>4. Precompile assets</h1>

Run the following command to precompile assets. 

```
rails assets:precompile
```

<h1>5. Run the server</h1>
Run the following command to start the rails server:

```
./bin/dev
```
<h1>6. Create Account and Register Client</h1>

Go to `/create-account` and make an account. 

After you make your account, paste the following into the terminal: 

```
curl --location -v --request POST 'http://localhost:3000/register?client_name=App&description=app&client_uri=http%3A%2F%2Flocalhost%3A3000&redirect_uris[]=http%3A%2F%2Flocalhost%3A3000%2Fcallback&scopes=openid%20email'
```

<h1>7. Create the par request</h1>

To reproduce the issues with `/par`, copy the curl request below. Be sure to replace the `Base64EncodedClientId:Secret` placeholder in the Authorization header and `client_id` placeholder in the body.
```
curl --location 'http://localhost:3000/par' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--header 'Authorization: Basic {Base64EncodedClientId:Secret}' \
--data-urlencode 'client_id={client_id}' \
--data-urlencode 'redirect_uri=http://localhost:3000/callback' \
--data-urlencode 'response_type=code' \
--data-urlencode 'scope=openid email' \
--data-urlencode 'state=af0ifjsldkj' \
--data-urlencode 'code_challenge=pq5ohK31MZblCNu9CYm23tCyjkWIjQI45-0S6RJyPeQ' \
--data-urlencode 'code_challenge_method=S256'
```

<h1>8. Route to Authorize Endpoint</h1>

Take the `request_uri` in the response from the curl request and paste the link below into your browser. Again, be sure to replace the placeholders in the link with your appropriate `client_id` and `request_uri`.

```
http://localhost:3000/authorize?client_id={client_id}&request_uri={request_uri}
```


