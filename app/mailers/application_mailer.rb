class ApplicationMailer < ActionMailer::Base
  default from: ENV["GMAIL_USERNAME"]
  smtp_settings = {
    :address => ENV["SMTP_ADDRESS"],
    :domain => ENV["SMTP_DOMAIN"],
    :port  => ENV["SMTP_PORT"],
    :user_name => ENV["GMAIL_USERNAME"],
    :password =>  ENV["GMAIL_PASSWORD"],
    :authentication => "plain",
    :enable_starttls_auto => true
  }
  layout "mailer"
end
