require "openssl"

PRIV_KEY = OpenSSL::PKey::RSA.new(File.read(Rails.root.join("tmp", "rsaprivkey.pem")))
PUB_KEY = OpenSSL::PKey::RSA.new(File.read(Rails.root.join("tmp", "rsapubkey.pem")))

class RodauthOauth < RodauthMain
  configure do

    # Oauth Authorization Features
    enable :oidc, :oidc_dynamic_client_registration, :oauth_authorization_code_grant,
           # :oauth_jwt_jwks
           # :oauth_pkce,
           # :oauth_token_introspection,
           :oauth_pushed_authorization_request,
           # :oauth_jwt_bearer_grant,
           # :oauth_jwt_secured_authorization_response_mode,
           # :oauth_jwt_secured_authorization_request,
           # :oidc_self_issued,
           :oidc_rp_initiated_logout
    # See the Rodauth documentation for the list of available config options:

    # oauth_response_types_supported "code"
    # oauth_response_mode "form_post"
    # list of OIDC and OAuth scopes you handled
    # oauth_application_scopes %w[openid email profile posts.read]
    login_return_to_requested_location? true

    secret_matches? { |oauth_application, secret| Argon2::Password.verify_password(secret, oauth_application[oauth_applications_client_secret_hash_column]) }
    secret_hash { |secret| Argon2::Password.create(secret) }

    check_csrf? false

    # require_authorizable_account false
    # oauth_require_pushed_authorization_requests true
    oauth_application_scopes %w[]

    oauth_account_ds { |id| Account.where(account_id_column => id) }
    oauth_application_ds { |id| OAuthApplication.where(oauth_applications_id_column => id) }

    if Rails.env.development?
      oauth_valid_uri_schemes %w[http https]
    end

    # oauth_require_pkce true
    oauth_jwt_keys("RS256" => PRIV_KEY)
    oauth_jwt_public_keys("RS256" => PUB_KEY)

    # oidc_authorize_on_prompt_none? { |_account| true }
    # oauth_request_object_signing_alg_allow_none true
    # oauth_acr_values_supported { super() | %w[1 2] }
    # oauth_jwt_jws_algorithms_supported { super() | %w[RS256] }

    # jwks_set [{
    #             "p": "wY-Fu_w8oWeZ0kJji7o4eV5119BRZ4G36ilz7oOm4JCoEWjEBJrm_maNjIMXqs7sk8juosD7dxHJOkWioBXu8IFYvTjnK-8wRzxeigEFSQZjezNyPwOT4nAP6lQWO6JFqTxnijYV38vQd-jc-67NDOuAPBVdRUtwEmhJVk09rmPzpA0C93dncCMXTmCApAjeCCUj0WGj46a18iDtGc6UToldg-dxMKrx63bqX5wv_NB_O9A_k_p9qrnN6bj5jm1EqG7iYvBn3-IUSwlN6CX3pBFFQNrDI1oheFB8vOhcXbYWoR_aY53cNYr1Eyurzqw4t6udpn8lR6Dn44B-2DyjHQ",
    #             "kty": "RSA",
    #             "q": "tFVEAZkrhLVF7QrDHoLm8OuA5lX5a9coGJlVXNMITxH3-711pOWJ-RYb77yXSHfGO60htuRAw1HI7a01UMC7KZCq1KGHa5isAtkYA2jKSIbeYI_mP74rrPt_yYbxj5rZxQRqKxfEow2It4pMH1RsEMWdSTa-PD7hy_CMnzkbmAs_hH_pssDBG9CouD5WSTsTRz4zktC4EdvbBQ74VlSztCtuXbS_ccjnTe8lCBHWkapa4QsRu4BdHCOF1eNZyr6uw5XUfoKa4rrYyIoE8REUNtYZVceVLc1mai7GjQHamb8zDCxTR21rte73qQJHrP3kLuzmG5JJ52NvalA4nA3n8Q",
    #             "d": "A70XsI85qrwFuwwoQRgbgbBt3dKraKyxiUJ3NZ78kTYvztLHe3reT5ThhOxaDyOM9_6lb6ufpnIcXFuG4cr08rH510aWqVGEDvullVaq_NSIrahnl8F0jDRqTRHtaWbJJze98ktY6tsDrt9Kv4Crmy7v0mAKEMvNiZlTrWIjsb_b6DMzNYeimcvhedKdjpQV4xfRqKeTBIP-HNihMqo143cYDR37Ky56LXsMqcybfDqw7YBaSWqqJGEXR_Rg5YuqWNuWrTcdWFie50ng2_mE-M5zH9Xwmqq64ngv43mBvITcfG619Hn5Fn8DrSyN4biQEB-dq-BNDCbzSa6NaxVSwrghrz1xP4wc1-w29Y3CtG6sOydV_I5bOiTuwWizi4h9GWObuw0UL3QYPAaG1YhwNl1xGcZ35z3FSyCknLUnhXUZvg4_DkRgokGamlRL351qkc739IRj50hncLued4ftQ94UcO7w3N7lKN3sKTN9b89We-Z_YfNb-wKliHlI1hCJgTIjJfcB2-H0vn9XRfPRz_8wJLxzo1Llmt5ONHotLgZE72bhEQeQVJYpKCaOmAfaNd3QC1hnGndY_9Q8VJQAlNmrl8ZBop-6kU1k9rtFRl9LUXxq3dFAXRyEdObsDZyqFQdmTNN2lEowuFShiU3nUhCtyR68YEK2lvB1ZmlCp0E",
    #             "e": "AQAB",
    #             "use": "sig",
    #             "qi": "omVouB5CXDgNmPEd6M2rGAWqDao_uvzCj2ahq6nGrAJwewUpccsKSoX4ec96PvmwNlywpJ1zHaUw5P2DN4XRNX-AYwq0f0pFCxiXr5l0sA6UPtTf6kkjldDWZOzZxVdbECYt-0DWYbTB-8UVh4xLsAdDPU-_Wu6Cg8qlQDsnvqrBPJKVESzpiCxyZO8lPSXmKSNSvO9yo0OhD8TF380U6I5dM_qrBxVIedeP-vARDXmN3TEqFuEqrloKMIXSPZTDPa1GvMPbLR3gelQipGtk1tAKnh0vn3l-0bbbAVxpNvUnSb8iFr3XbCTzoGt_rju6GkNcb68g35A-G9Qrdms8bQ",
    #             "dp": "sLACtAcckXmwwG450GK9Co1fpRFUPdf7upT8bs1el790q--mf02nbrLOo2VHRXR-IKlarNbctWRW_Ap02Q0sf_6Ssqgu3bwJpf1Fj1Q_LtnWFbBwqXTqmdkRriXz5ZvLWyD4owGcFh5Gvi6jClwWcaTH-hDGg2rJcwQfbdg0Zex3i1kVTDCDC6n8rF8LDrHJfEXq3Eb1A-o6TDwiM1yPpOFfq_ipKnPhQb_bUkuQwobQnhkK8FnIPQA_JwrjQ1AbTGVr4kTxjT0iqRxlMXT3CZ16Siln1-1-gAV42DiDmEePgKVDBYwkZ_vgmw9SVeaAH6lYz5NgxHHtO48gOuOfQQ",
    #             "alg": "RS256",
    #             "dq": "BwIlUXaVdWPiItM_QAlQVl6gtNL0jZdgDIyw_5mIAiKto4_6aX0uPor4bPnEW2GkR59z5vq59Z1RFMOdhdTnsTbZHMhmTe0yCi5Lu54v9l-fdttA1ItcgS2JCtj-vFkFS53fTmtvDfMxtAhg6vrJ9GDpuvWwsQC0n8tqe6mpg-A0CL874uGQ37bdR8PMcK4mEqe9M2B_Ntp3iZvKVPiXv1HMyUGECzUGoEXCB61BzIl1XLekacgUVkqfIe_jBnkgq6NADeafhF5EnI6YoJHMWlE5zNaj2pLX_xpwRvyidUU0A64ZUnZTME_oOh5HkNaRLq2wR7QIwUBhDR2vBYmY0Q",
    #             "n": "iFliGumqhLpnSw9tutDuctA2dJu9zidZUMZe4_af3jO3NF3IMVdMR689X0mBtUupUdwE44Q_jAD4Oz9JRRHpugqeTvvcFj-7sMAdxaCZNxBQFvKk_bo87J7FqSHU4a8pdM1OyQ1r3vCMcvYdvqut7CIMcD0JGA6UEqSUi0ynhEgJIDYM7ONiW9AHpoxgeQouUyeRhM48IXq_TOQ6BeYms-yOfV590KcoBnFrG6z8jsULSB_hexQGIcvPtnE79vZwnh79aGSoBoxbuykxIK1dpHVgfGBdijvE6FymWD0lL4YynXIqVlH-pGm6ypxJQ3JdRDFDBrERBGp64-ml6eHGvjRG_uVmQE_ERObjabk73jjzLmH_hyKGGyeATmqsVgLZvVs7-RnLKn02TdmKMsOoik12PLx3duAA1PXofnFeqHsdJeah8tsvV0QUmUxWkO127FEdu_svHFhkLEYwq4lAOkMhw801UtrkthSr4Is4Hk0sPAEikrU-TIte1hNX0Gyx1Tn6xoIFSNvRn9eTfA0AGw0Fl5VBkgcFvPkMrYF4EsttF6S0QK4LBQeAUS47bedEP_mDlcE6BEv8-nWyYVtU0NcRmV66yxw_VaLthL4mCvBfARjkes8dU0zaWrTA1cTXZhhU5owLrFIw6spoEL_93-2OFiNwT9rxxZMZWjy9uU0"
    #           }]

    get_oidc_param do |account, param|
      @profile ||= Profile.find_by(account_id: account[:id])
      case param
      when :email
        account[:email]
      when :email_verified
        account[:status] == "verified"
      when :name
        @profile.name
      end
    end

    before_register do
      secret_code = ENV["HTTP_AUTHORIZATION"]
      authorization_required unless secret_code = "12345678"
      @oauth_application_params[:account_id] = _account_from_login("amir@amirs.io")[:id]
    end
    # rodauth.before_authorize_route do
    #   r.post do
    #     unless request.params['response_type'] do
    #       request.params['response_type'] = "code"
    #     end
    #     end
    #   end
    # end

    # before_authorize_route do
    #   unless (request.env['REQUEST_METHOD'] = 'POST')
    #     request.env['REQUEST_METHOD'] = 'POST'
    #   end
    #   if request.post?
    #     if (request_uri = param_or_nil("request_uri"))
    #       code = request_uri.delete_prefix("urn:ietf:params:oauth:request_uri:")
    #       table = oauth_pushed_authorization_requests_table
    #       ds = db[table]
    #
    #       pushed_request = ds.where(
    #         oauth_pushed_authorization_requests_oauth_application_id_column => oauth_application[oauth_applications_id_column],
    #         oauth_pushed_authorization_requests_code_column => code
    #       ).where(
    #         Sequel.expr(Sequel[table][oauth_pushed_authorization_requests_expires_in_column]) >= Sequel::CURRENT_TIMESTAMP
    #       ).first
    #
    #       redirect_response_error("invalid_request") unless pushed_request
    #
    #       authorize_params = URI.decode_www_form(pushed_request[oauth_pushed_authorization_requests_params_column])
    #
    #       authorize_params.each do |k, v|
    #         request.params[k.to_s] = v
    #       end
    #     end
    #   end
    # end

  end
end
