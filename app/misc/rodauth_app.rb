require 'securerandom'
require 'base64'
require 'openssl'

class RodauthApp < Rodauth::Rails::App
  # primary configuration
  configure RodauthOauth

  route do |r|
    r.rodauth

    rodauth.require_authentication

    r.on "callback" do
      r.post do
        # if r.params["error"]
        #   flash[:error] = "Authorization failed: #{r.params['error_description'] || r.params['error']}"
        #   r.redirect "/"
        # end
        #
        # session_state = session.delete("state")
        #
        # if session_state
        #   state = request.params["state"]
        #   if !state || state != session_state
        #     flash[:error] = "state doesn't match, CSRF Attack!!!"
        #     r.redirect "/"
        #   end
        # end

        code = r.params["code"]
        client_id = ENV["CLIENT_ID"]
        redirect_uri = ENV["REDIRECT_URI"]
        client_secret = ENV["CLIENT_SECRET"]

        response = json_request(:post, "http://localhost:3000/token",
                                headers: {
                                  "Authorization": "Basic " + Base64.urlsafe_encode64("#{client_id}:#{client_secret}")
                                },
                                params: {
                                  "grant_type" => "authorization_code",
                                  "client_id" => client_id,
                                  "code" => code,
                                  "redirect_uri" => redirect_uri,
                                  # "state" => "af0ifjsldkj",
                                  # "code_verifier" => "wUzsuL_tmSROjwgxmWPe4WWlLpziKzi0wyjd4kLFn2A"
                                })

        session["access_token"] = response[:access_token]
        session["refresh_token"] = response[:refresh_token]

        r.redirect "/"
      end
    end

    r.root do
      view inline: <<~HTML
        <% if rodauth.logged_in? %>
        <p class="lead">
          You are now logged in to the OpenID authentication server. You're able to add client applications, and authenticate with your account.
        </p>
        <% else %>
          <p class="lead">
            This is the demo authentication server for <a href="https://gitlab.com/os85/rodauth-oauth">Roda Oauth - Open ID Connect</a>.
            Roda Oauth extends Rodauth to support the OAuth 2.0 authorization protocol, while adhering to the same principles of the parent library.
          </p>
          <p class="lead">In the authentication server, you can setup your account, and also register client applications.</p>
          <p class="text-center">
            <a class="btn btn-outline-primary btn-padded" href="/login">Login</a>
            <a class="btn btn-outline-secondary btn-padded" href="/create-account">Sign Up</a>
          </p>
          <footer class="lead">This demo site is part of the Rodauth repository, so if you want to know how it works, you can <a href="https://gitlab.com/os85/rodauth-oauth/tree/master/examples">review the source</a>.</footer>
        <% end %>
      HTML
    end

    r.on "rotate-keys" do
      r.get do
        jws_key = OpenSSL::PKey::RSA.generate(2048)
        jws_public_key = jws_key.public_key

        PRIV_KEY.unshift(jws_key)
        PUB_KEY.unshift(jws_public_key)

        "rotated"
      end
    end
  end

  private

  # def verify_openid_session
  #   @profile = if (expiration_time = session["token_expires"])
  #
  #                if expiration_time < Time.now.utc.to_i
  #                  session.delete("info")
  #                  session.delete("id_token")
  #                  session.delete("access_token")
  #                  session.delete("refresh_token")
  #                  session.delete("token_expires")
  #                  {}
  #                else
  #                  session["info"]
  #                end
  #              else
  #                {}
  #              end
  # end

  def json_request(meth, uri, headers: {}, params: {})
    uri = URI(uri)
    http = Net::HTTP.new(uri.host, uri.port)
    case meth
    when :get
      request = Net::HTTP::Get.new(uri.request_uri)
      request["accept"] = "application/json"
      headers.each do |k, v|
        request[k] = v
      end
      response = http.request(request)
      raise "Unexpected error on token generation, #{response.body}" unless response.code.to_i == 200

      JSON.parse(response.body, symbolize_names: true)
    when :post
      request = Net::HTTP::Post.new(uri.request_uri)
      request.body = JSON.dump(params)
      request["content-type"] = "application/json"
      request["accept"] = "application/json"
      headers.each do |k, v|
        request[k] = v
      end
      response = http.request(request)
      raise "Unexpected error on token generation, #{response.body}" unless response.code.to_i == 200

      JSON.parse(response.body, symbolize_names: true)
    end
  end
end
